<?php


namespace Tangle;

/** Define THE_TANGLE_PATH as this file's directory */
if ( ! defined( 'THE_TANGLE_PATH' ) ) {
    define( 'THE_TANGLE_PATH', dirname( __FILE__ ) . '/' );
}

use Firebase\JWT\JWT;

class TangleCookie
{

    public static $version = '0.0.4';
    private $strict, $tangleCookieName, $cookie_hash, $config_container;
    public $tangle = array();
    private $encoded, $secret_key;


    /**
     * TangleCookie constructor.
     * @param bool $encoded
     */
    public function __construct($encoded = true) {
        $this->encoded = $encoded;
//        $this->cookie_hash = uniqid(md5(mt_rand()));
        $this->config_container = $this->get_config();
        if (isset($this->config_container) && !empty($this->config_container)) {
            $this->secret_key = $this->config_container['jwt']['key'];
        }
        isset($_SERVER['HTTP_HOST']) ? $tag = str_replace(":", "", $_SERVER['HTTP_HOST']) : $tag = "0";
        $tag = str_replace(".", "", $tag); //todo
        $this->tangleCookieName = "tangle_tag_$tag";
        $this->tangle = self::getTangleCookie();
        self::init();
    }

    /**
     * @param $key
     * @return bool
     */
    public function inTangle($key) {
        return isset($this->tangle[$key]) ? true : false;
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    public function addTangle($key, $value) {
        if (!self::inTangle($key)) {
            $this->tangle[$key] = $value;
            self::setTangleCookie($this->tangle);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $key
     * @return bool
     */
    public function rmTangle($key) {
        if (self::inTangle($key)) {
            unset($this->tangle[$key]);
            $this->tangle = array_values($this->tangle);
            self::setTangleCookie($this->tangle);
            return true;
        } else {
            return false;
        }
    }

    public function updateTangle($key, $value) {
        if (self::inTangle($key)) {
            $this->tangle[$key] = $value;
            self::setTangleCookie($this->tangle);
            return true;
        } else {
            return false;
        }
    }


    /**
     * @codeCoverageIgnore
     */
    protected static function init() {
        if ( !defined('TANGLE_LOADED') ) {
            define('TANGLE_LOADED', true);
        }
    }


    /**
     * @return mixed|array
     */
    protected function getTangleCookie() {
        $cookie_name = $this->tangleCookieName;
        if ($this->encoded) {
            isset($_COOKIE[$cookie_name]) ? $data = $this->decode($_COOKIE[$cookie_name]) : $data = [];
        }
        return isset($data) ? (array) $data : [];
    }

    /**
     * @param $data
     */
    protected function setTangleCookie(array $data) {
        $cookie_name = $this->tangleCookieName;
        if ($this->encoded) {
            $data = $this->encode($data);
        }
        setcookie($cookie_name, $data, time() + (86400 * 365), '/');
    }

    protected function rmLngCookie() {
        $cookie_name = $this->tangleCookieName;
        unset($_COOKIE[$cookie_name]);
        $res = setcookie($cookie_name, '', time() - 3600);
    }

    /**
     * @param $url
     * @param bool $permanent
     */
    protected function redirect($url, $permanent = false) {
        header('Location: ' . $url, true, $permanent ? 301 : 302);
        exit();
    }

    /**
     * @param $url
     * @param $key
     * @return mixed|string
     */
    protected function removeParameterFromUrl($url, $key) {
        $parsed = parse_url($url);
        $path = $parsed['path'];
        unset($_GET[$key]);
        if(!empty(http_build_query($_GET))){
            return $path .'?'. http_build_query($_GET);
        } else return $path;
    }


    protected function decode($data) {
        try {
            $decoded = JWT::decode($data, $this->secret_key, array('HS256'));
        } catch (\Error $e) {

        }
        finally {
            if (isset($decoded)){
                return $decoded;
            } else {
                return null;
            }
        }

    }

    protected function encode($token) {
        $_token = JWT::encode($token, $this->secret_key, 'HS256');
        return isset($_token) ? $_token : null;
    }


    protected function get_config($config_file = THE_TANGLE_PATH.'.tangle.ini.php') {
        $ini_array = [];
        if (self::exists($config_file)) {
            $ini_array = parse_ini_file($config_file, true);
        }
        if (!isset($ini_array['jwt']['key']) || empty($ini_array['jwt']['key']) || $ini_array['jwt']['key'] == 'changed_it_will_be') {
            $this->update_config(THE_TANGLE_PATH.'.tangle.ini.php', 'jwt', 'key', uniqid(md5(mt_rand())));
            if (self::exists($config_file)) {
                $ini_array = parse_ini_file($config_file, true);
            }
        }
        return $ini_array;
    }


    protected function exists($fn) {
        $filename = strip_tags($fn); //todo ?
        return file_exists($filename) ? true : false;
    }

    // todo POC - not for use
    protected function update_config($config_file, $section, $key, $value) {
        $config_data = parse_ini_file($config_file, true);
        $config_data[$section][$key] = $value;
        $new_content = ";<?php
;echo 'Action noted';
;die();
;/*\n";
        foreach ($config_data as $section => $section_content) {
            $section_content = array_map(function($value, $key) {
                return "$key=\"$value\"";
            }, array_values($section_content), array_keys($section_content));
            $section_content = implode("\n", $section_content);
            $new_content .= "[$section]\n$section_content\n";
        }
        $new_content .= ";*/
;?>\n";
        file_put_contents($config_file, $new_content);
    }

}
