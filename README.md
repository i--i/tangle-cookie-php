Tangle-Cookie PHP
====

A [Tangle-Cookie][tanglecookieweb] is released under the open
source [MIT-license][MIT-license].

Tangle-Cookie PHP library
Simple abstraction of cookie based JWT encoded 

Installation
------------

`$ composer require tangle/tangle-cookie-php dev-master`

Usage
-----
Just instantiate it as shown in example
```
$tangle = new \Tangle\TangleCookie();
```
Then simply entangle yourself with
```
//get tangle
$tangle->tangle;

//check if key is already in tangle
$tangle->inTangle($key);

//add to tangle
$tangle->addTangle($key, $value);

//remove from tangle
$tangle->rmTangle($key);

//update tangle
$tangle->updateTangle($key, $value);
```

Reporting issues
----------------

See our [Contributing to Tangle-Cookie][contributing] guide.

Support
-------

Have a question? Want to chat? Run into a problem? See our [community][support]
page.

---


[tanglecookieweb]: https://tangle.iridiumintel.com
[MIT-license]: http://opensource.org/licenses/mit-license.php
[docs]: https://tangle.iridiumintel.com/docs
[support]: https://tangle.iridiumintel.com/community
[contributing]: https://gitlab.com/litstrings.io/litstrings-php/CONTRIBUTING.md